package net.jackcroft.potions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Jack
 *
 */
public class PotionOptions {

	public void findListCombinations(int numEffects, ArrayList<Ingredient> ingredients, IngredientList list) {
		int combinations = 0;
		for (int i = 0; i < ingredients.size(); i++) {
			for (int j = i + 1; j < ingredients.size(); j++) {
				if (list.sharedEffects(ingredients.get(i), ingredients.get(j), numEffects).size() != 0) {
					System.out
							.println("Combination: " + ingredients.get(i).getName() + " + "
									+ ingredients.get(j).getName() + " yields "
									+ list.sharedEffects(ingredients.get(i), ingredients.get(j), numEffects).toString()
											.substring(1, list
													.sharedEffects(ingredients.get(i), ingredients.get(j), numEffects)
													.toString().length() - 1));
					combinations++;
				}
			}
		}
		if (combinations == 0)
			System.out.println("No combinations available");
	}

	public void findIngredientCombinations(int numEffects, Ingredient ingredient, IngredientList list) {
		HashMap<Ingredient, String> commonIngredients = list.getCommonIngredients(ingredient, numEffects);
		Set<Ingredient> keys = commonIngredients.keySet();
		Iterator<Ingredient> i = keys.iterator();

		while (i.hasNext()) {
			Ingredient next = i.next();
			System.out.println(next.getName() + ": " + commonIngredients.get(next));
		}

		if (keys.isEmpty())
			System.out.println("No combinations available");
	}

	public void findEffectCombinations(int numEffects, String effect, IngredientList list) {
		ArrayList<Ingredient> effectIngredients = list.getIngredientsWithEffect(effect, numEffects);

		for (int i = 0; i < effectIngredients.size(); i++) {
			System.out.println(effectIngredients.get(i).getName());
		}

		if (effectIngredients.size() == 0)
			System.out.println("No combinations available");
	}

	public static void main(String[] args) {

	}
}