package net.jackcroft.potions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Class that keeps track of all the user's ingredients as well as all the
 * ingredients
 * 
 * @author Jack Croft
 *
 */
public class IngredientFileStore {

	private ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
	private ArrayList<Ingredient> currentIngredients = new ArrayList<Ingredient>();
	private IngredientList list;
	private String ingredientFileName;
	private String currentIngredientFileName;

	/**
	 * Creates a new ingredient file store and loads up all the ingredients
	 * 
	 * @param isOblivion
	 *            Whether or not the file store is for Oblivion or Skyrim
	 */
	public IngredientFileStore(boolean isOblivion) {
		if (isOblivion) {
			ingredientFileName = "OblivionIngredients.txt";
			currentIngredientFileName = "CurrentOblivionIngredients.txt";
		} else {
			ingredientFileName = "SkyrimIngredients.txt";
			currentIngredientFileName = "CurrentSkyrimIngredients.txt";
		}
		ingredients = loadFiles();
		list = new IngredientList(ingredients);
	}

	/**
	 * Loads up a list of ingredients from file
	 * 
	 * @return an ArrayList of ingredients from file
	 */
	private ArrayList<Ingredient> loadFiles() {
		String name = "";
		String effect1 = "";
		String effect2 = "";
		String effect3 = "";
		String effect4 = "";
		ArrayList<Ingredient> ingredientsFromFile = new ArrayList<Ingredient>();

		try {
			Scanner reader = new Scanner(new File(ingredientFileName));
			int currentLine = 0;
			while (reader.hasNextLine()) {
				if (currentLine == 0) {
					name = reader.nextLine();
					currentLine++;
				} else if (currentLine == 1) {
					effect1 = reader.nextLine();
					currentLine++;
				} else if (currentLine == 2) {
					effect2 = reader.nextLine();
					currentLine++;
				} else if (currentLine == 3) {
					effect3 = reader.nextLine();
					currentLine++;
				} else if (currentLine == 4) {
					effect4 = reader.nextLine();
					Ingredient i = new Ingredient(name, effect1, effect2, effect3, effect4);
					ingredientsFromFile.add(i);
					currentLine = 0;
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ingredientsFromFile;
	}

	/**
	 * Returns a list of text files within the workspace
	 * 
	 * @return a list of text files within the workspace
	 */
	public File[] listTextFiles() {
		File dir = new File(".");

		return dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				return filename.endsWith(".txt");
			}
		});
	}

	/**
	 * Returns the ingredients currently in the file store
	 * 
	 * @return the ingredients currently in the file store
	 */
	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}

	/**
	 * Returns the user's current ingredients from file
	 * 
	 * @return the user's current ingredients from file
	 */
	public ArrayList<Ingredient> getCurrentIngredients() {
		return currentIngredients;
	}

	/**
	 * Prints the given ingredient to file
	 * 
	 * @param filename
	 *            the file to print to
	 * @param ingredientName
	 *            the name of the ingredient to print
	 */
	public void printToFile(String filename, String ingredientName) {
		try {
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
			writer.println(ingredientName);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds a new ingredient to the user's current ingredients
	 * 
	 * @param ingredient
	 *            The ingredient to be added
	 */
	public void addCurrentIngredient(String ingredient) {
		ingredient = ingredient.toLowerCase();
		readCurrentIngredients();
		if (currentIngredients.contains(list.getIngredient(ingredient)))
			return;

		File[] files = listTextFiles();
		File current = null;
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().equals(currentIngredientFileName))
				current = files[i];
		}
		if (current != null)
			current.delete();
		currentIngredients.add(list.getIngredient(ingredient));
		Collections.sort(currentIngredients);
		new File(currentIngredientFileName);
		for (int i = 0; i < currentIngredients.size(); i++) {
			printToFile(currentIngredientFileName, currentIngredients.get(i).getName());
		}
		readCurrentIngredients();
	}

	/**
	 * Reads in the user's current ingredients from file
	 */
	public void readCurrentIngredients() {
		try {
			Scanner reader = new Scanner(new File(currentIngredientFileName));
			while (reader.hasNextLine()) {
				Ingredient i = new Ingredient(list.getIngredient(reader.nextLine()));
				if (!currentIngredients.contains(i))
					currentIngredients.add(i);
			}
			reader.close();
		} catch (IOException e) {
			if (e.getLocalizedMessage()
					.equals("CurrentOblivionIngredients.txt (The system cannot find the file specified)"))
				currentIngredients = new ArrayList<Ingredient>();
			else if (e.getLocalizedMessage()
					.equals("CurrentSkyrimIngredients.txt (The system cannot find the file specified)"))
				currentIngredients = new ArrayList<Ingredient>();
			else
				e.printStackTrace();
		}
	}

	/**
	 * Deletes an ingredient from the user's current ingredients
	 * 
	 * @param ingredient
	 *            the ingredient to be deleted
	 * @return whether or not the delete was successful
	 */
	public boolean deleteIngredient(Ingredient ingredient) {
		readCurrentIngredients();
		ArrayList<Ingredient> temp = new ArrayList<Ingredient>();

		for (Ingredient i : currentIngredients)
			temp.add(new Ingredient(i));

		if (!currentIngredients.contains(ingredient))
			return false;

		File[] files = listTextFiles();
		File current = null;
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().equals(currentIngredientFileName))
				current = files[i];
		}
		current.delete();
		currentIngredients = new ArrayList<Ingredient>();
		temp.remove(ingredient);
		Collections.sort(temp);
		new File(currentIngredientFileName);
		for (int i = 0; i < temp.size(); i++) {
			printToFile(currentIngredientFileName, temp.get(i).getName());
		}
		readCurrentIngredients();
		return true;
	}
}