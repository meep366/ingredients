package net.jackcroft.potions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class that maintains the user's current ingredient list
 * @author Jack Croft
 */
public class IngredientList {
	private ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
	private ArrayList<String> effects = new ArrayList<String>();

	/**
	 * Creates a new ingredient list from an ArrayList of ingredients
	 * @param list the list of ingredients to be included in the IngredientList
	 */
	public IngredientList(ArrayList<Ingredient> list) {
		ingredients = list;
		for (int i = 0; i < ingredients.size(); i++) {
			for (int j = 0; j < ingredients.get(i).getEffects().size(); j++) {
				if (!effects.contains(ingredients.get(i).getEffect(j)))
					effects.add(ingredients.get(i).getEffect(j));
			}
		}
	}

	/**
	 * Returns whether or not an ingredient is valid
	 * @param ingredient The ingredient to be validated
	 * @return whether or not an ingredient is valid
	 */
	public boolean isValidIngredient(String ingredient) {
		for (int i = 0; i < ingredients.size(); i++) {
			if (ingredients.get(i).getName().equalsIgnoreCase(ingredient)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns an ingredient from the list given the string name of the ingredient
	 * @param name the name of the ingredient to be given
	 * @return the ingredient from the list
	 */
	public Ingredient getIngredient(String name) {
		for (int i = 0; i < ingredients.size(); i++) {
			if (ingredients.get(i).getName().equalsIgnoreCase(name))
				return ingredients.get(i);
		}
		return null;
	}

	/**
	 * Returns a list of shared effects between two given ingredients
	 * @param one first ingredient to be compared
	 * @param two second ingredient to be compared
	 * @param numEffects the maximum number of common effects to be found
	 * @return a list of shared effects between two given ingredients
	 */
	public ArrayList<String> sharedEffects(Ingredient one, Ingredient two, int numEffects) {
		ArrayList<String> sharedEffects = new ArrayList<String>();
		for (int i = 0; i < numEffects; i++) {
			for (int j = 0; j < numEffects; j++) {
				if (one.getEffects().get(i).equals(two.getEffects().get(j)))
					sharedEffects.add(one.getEffect(i));
			}
		}
		return sharedEffects;
	}
	
	/**
	 * Returns a map of ingredients with effects in common to the effect in common
	 * @param ingredient 
	 * @param numEffects
	 * @return
	 */
	public HashMap<Ingredient, String> getCommonIngredients(Ingredient ingredient, int numEffects) {
		HashMap<Ingredient, String> commonIngredients = new HashMap<Ingredient, String>();

		for (int i = 0; i < ingredients.size(); i++) {
			if (!ingredients.get(i).equals(ingredient)) {
				ArrayList<String> sharedEffects = sharedEffects(ingredient, ingredients.get(i), numEffects);
				for (int j = 0; j < sharedEffects.size(); j++) {
					commonIngredients.put(ingredients.get(i), sharedEffects.get(j));
				}
			}
		}

		return commonIngredients;
	}

	public ArrayList<Ingredient> getIngredientsWithEffect(String effect, int numEffects) {
		ArrayList<Ingredient> effectIngredients = new ArrayList<Ingredient>();

		for (int i = 0; i < ingredients.size(); i++) {
			for (int j = 0; j < numEffects; j++) {
				if (ingredients.get(i).getEffect(j).equalsIgnoreCase(effect))
					effectIngredients.add(ingredients.get(i));
			}
		}

		return effectIngredients;
	}

	public boolean isValidEffect(String effect) {
		for (int i = 0; i < effects.size(); i++) {
			if (effects.get(i).equalsIgnoreCase(effect))
				return true;
		}
		return false;
	}

	public static void main(String[] args) {

	}
}