package net.jackcroft.potions;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Jack Croft
 *
 */
public class Main {

	//TODO: Replace isOblivion with Enum
	//TODO: Replace FileStore file URL with relative URL
	//TODO: Create enum for effects? (Can we create a valid enum from a string?)
	//TODO: Create enum for valid ingredients and effects?
	//combo box should auto complete by first ingredient name
	//hitting enter should also add ingredient, same as button press, in either box
	//count should return to 1 after adding each ingredient
	//clicking on count box should highlight current count, so that backspace is unnecessary

	private Scanner userInput;
	private IngredientFileStore handler;
	private PotionOptions options;
	private IngredientList list;
	private boolean isOblivion;

	/**
	 * Sets up main class and runs the program
	 * @param iO Whether or not the the program is running for Oblivion or Skyrim
	 */
	public Main(boolean iO) {
		isOblivion = iO;
		userInput = new Scanner(System.in);
		options = new PotionOptions();
		handler = new IngredientFileStore(isOblivion);
		handler.readCurrentIngredients();
		list = new IngredientList(handler.getIngredients());
		run();
	}

	private void getCurrentIngredients() {
		handler.readCurrentIngredients();
		ArrayList<Ingredient> current = handler.getCurrentIngredients();
		for (int i = 0; i < current.size(); i++) {
			System.out.println(current.get(i).getName());
		}
		System.out.println(current.size() + " ingredients total");
		System.out.println('\n' + "Enter ingredients separted by enter then type 'done' to stop entering ingredients");
	}

	private void getIngredientEffects(String[] words) {
		String item = "";
		for (int i = 1; i < words.length; i++) {
			item += words[i] + " ";
		}
		item = item.trim();
		if (list.isValidIngredient(item)) {
			for (int i = 0; i < list.getIngredient(item).getEffects().size(); i++)
				System.out.println(list.getIngredient(item).getEffect(i));
		}
		System.out.println('\n' + "Enter ingredients separted by enter then type 'done' to stop entering ingredients");
	}

	private void listIngredientCombinations(String[] words) {
		String item = "";
		for (int i = 1; i < words.length; i++) {
			item += words[i] + " ";
		}
		item = item.trim();
		if (list.isValidIngredient(item)) {
			int currentLevel = getCurrentLevel();
			options.findIngredientCombinations(currentLevel, list.getIngredient(item), list);
			System.out.println(
					'\n' + "Enter ingredients separted by enter then type 'done' to stop entering ingredients");
		} else if (list.isValidEffect(item)) {
			int currentLevel = getCurrentLevel();
			options.findEffectCombinations(currentLevel, item, list);
			System.out.println(
					'\n' + "Enter ingredients separted by enter then type 'done' to stop entering ingredients");
		} else {
			System.out.println("Not a valid ingredient or effect");
			System.out.println(
					'\n' + "Enter ingredients separted by enter then type 'done' to stop entering ingredients");
		}
	}

	private void deleteIngredientFromList() {
		System.out.println("Enter ingredients to delete and type done when you wish to stop deleting");
		boolean deleted = false;
		while (!deleted) {
			String deleteIngredient = userInput.nextLine();
			deleteIngredient = deleteIngredient.trim();
			if (deleteIngredient.equals("done")) {
				deleted = true;
				System.out.println(
						'\n' + "Enter ingredients separted by enter then type 'done' to stop entering ingredients");
			} else {
				boolean success = handler.deleteIngredient(list.getIngredient(deleteIngredient));
				if (success)
					System.out.println("Ingredient Deleted");
				else
					System.out.println("Ingredient Not In List");
			}
		}
	}

	private int getCurrentLevel() {
		if (!isOblivion)
			return 4;

		System.out.println("Enter current number of effects visible");
		int currentLevel = 0;
		boolean finished = false;
		while (!finished) {
			String number = userInput.nextLine();
			number = number.trim();
			if (number.matches("\\d+")) {
				currentLevel = Integer.valueOf(number);
				if (currentLevel > 4 || currentLevel < 1)
					System.out.println("Number of effects must be between 1 and 4");
				else
					finished = true;
			} else
				System.out.println("Input must be a number");
		}
		return currentLevel;
	}

	private void run() {
		System.out.println("Enter ingredients separted by enter then type 'done' to stop entering ingredients");
		System.out.println("You may type 'get' to get a list of all the ingredients you have");
		System.out
				.println("You may type 'find' followed by an ingredient to get a list of effects for that ingredient");
		System.out.println(
				"You may type 'list' followed by an ingredient or effect to get a list of all possible combinations");
		System.out.println("You may type 'delete' to delete an item in your list");
		boolean done = false;
		boolean listed = false;
		while (!done) {
			String input = userInput.nextLine();
			input = input.trim();
			String[] words = input.split("\\W");
			if (words.length > 1) {
				if (words[0].equals("list")) {
					listIngredientCombinations(words);
					listed = true;
				} else if (words[0].equals("find")) {
					getIngredientEffects(words);
					listed = true;
				}
			}
			if (!listed) {
				if (input.equalsIgnoreCase("done"))
					done = true;
				else if (input.equalsIgnoreCase("get")) {
					getCurrentIngredients();
				} else if (input.equalsIgnoreCase("delete")) {
					deleteIngredientFromList();
				} else if (input.equalsIgnoreCase("42")) {
					System.out.println(
							"That may be the answer to the question of the universe, but it is not a valid ingredient");
				} else {
					if (list.isValidIngredient(input)) {
						handler.addCurrentIngredient(input);
						System.out.println("Ingredient Added");
					} else {
						System.out.println("Invalid Ingredient");
					}
				}
			}
			listed = false;
		}
		handler.readCurrentIngredients();
		int currentLevel = getCurrentLevel();
		options.findListCombinations(currentLevel, handler.getCurrentIngredients(), list);
	}

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Welcome to Elder Scrolls Potion Maker");
		System.out.println("Please enter the name of the game you are playing");
		boolean done = false;
		boolean isOblivion = true;
		while (!done) {
			String game = userInput.nextLine();
			game = game.trim();
			if (game.equalsIgnoreCase("Oblivion")) {
				isOblivion = true;
				done = true;
			} else if (game.equalsIgnoreCase("Skyrim")) {
				isOblivion = false;
				done = true;
			} else
				System.out.println("Must be either Oblivion or Skyrim");
		}

		Main r = new Main(isOblivion);
		userInput.close();
	}
}