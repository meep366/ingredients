package net.jackcroft.potions;

import java.util.ArrayList;

/**
 * Class representing a single ingredient
 * @author Jack Croft
 *
 */
public class Ingredient implements Comparable<Ingredient> {

	private final String name;
	private final ArrayList<String> effects;

	/**
	 * Constructor for ingredient class that takes in effects separately
	 * @param n Name of the ingredient
	 * @param e1 First ingredient effect
	 * @param e2 Second ingredient effect
	 * @param e3 Third ingredient effect
	 * @param e4 Fourth ingredient effect
	 */
	public Ingredient(String n, String e1, String e2, String e3, String e4) {
		effects = new ArrayList<String>();
		name = n;
		effects.add(e1);
		effects.add(e2);
		effects.add(e3);
		effects.add(e4);
	}

	/**
	 * Constructor for ingredient class that takes in ArrayList of effects
	 * @param n Name of ingredient
	 * @param e ArrayList of ingredient effects
	 */
	public Ingredient(String n, ArrayList<String> e) {
		name = n;
		effects = e;
	}

	/**
	 * Constructor for ingredient class that takes in an existing ingredient
	 * @param ing The currently existing ingredient
	 */
	public Ingredient(Ingredient ing) {
		effects = new ArrayList<String>();
		name = ing.getName();
		for (int i = 0; i < ing.getEffects().size(); i++)
			effects.add(ing.getEffect(i));
	}

	/**
	 * Returns the list of ingredient effects
	 * @return the list of ingredient effects
	 */
	public ArrayList<String> getEffects() {
		return effects;
	}

	/**
	 * Returns the ith ingredient effect
	 * @param i Which ingredient effect to return 
	 * @return the ith ingredient effect
	 */
	public String getEffect(int i) {
		return effects.get(i);
	}

	/**
	 * Returns the name of the ingredient
	 * @return the name of the ingredient
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns whether or not the ingredient has the given effect
	 * @param effect the effect to check the ingredient for
	 * @return whether or not the ingredient has the given effect
	 */
	public boolean hasEffect(String effect) {
		for (int i = 0; i < effects.size(); i++) {
			if (effects.get(i).equalsIgnoreCase(effect))
				return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object value) {
		if (value instanceof Ingredient) {
			return ((Ingredient) value).getName().equalsIgnoreCase(this.getName());
		}
		return false;
	}

	@Override
	public String toString() {
		return name + ": " + effects.get(0) + ", " + effects.get(1) + ", " + effects.get(2) + ", " + effects.get(3);
	}

	@Override
	public int compareTo(Ingredient i) {
		return this.getName().compareToIgnoreCase(i.getName());

	}
}